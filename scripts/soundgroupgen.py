import sys
import os
from subprocess import call

#this should be run from inside the data directory

def writeFiles(dir):
	process = list();
	process.append(dir);
	fx = open('effectsfile.txt','w')
	fx2 = open('effectsfile2.txt','w')
	gp = open('groufile.txt','w');
	while(len(process) > 0):	
		dir = process.pop();
		files = os.listdir(dir)
		for e in files:
			if(os.path.isdir(os.path.join(dir,e))):
				process.append(os.path.join(dir,e))
				continue
			fn = os.path.splitext(e)
			if(fn[1] == '.wav'):
				fx.write("""CIwSoundSpec{
	name		" """ + os.path.join(dir,e) + """ "
	data		" """ + fn[0] + """ "
	vol 		1
	pitchMin	0.7
	pitchMax	1.3
}""")
				fx2.write(os.path.join(dir,e) + "\n")
				gp.write("""addSpec		""" + "\"" + os.path.join(dir,e) + "\"\n");
				

def main(argv):
    writeFiles(argv[0])
if __name__ == "__main__":
    main(sys.argv[1:])
